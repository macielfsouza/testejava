package br.com.blz.testApp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Inventory")
public class Inventory {
    
	
	@Column(name="quantity")
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id
	private Long quantity;
	
	@OneToMany
	private List<Warehouse> warehouse; 
	
	public Inventory(){
	
	}
	
	public Inventory(Long quantity, List<Warehouse> warehouse){
		
		this.quantity = quantity;
		this.warehouse = warehouse;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public List<Warehouse> getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(List<Warehouse> warehouse) {
		this.warehouse = warehouse;
	}

	@Override
	public String toString() {
		return "Inventory [quantity=" + quantity + ", warehouse=" + warehouse + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((warehouse == null) ? 0 : warehouse.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inventory other = (Inventory) obj;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (warehouse == null) {
			if (other.warehouse != null)
				return false;
		} else if (!warehouse.equals(other.warehouse))
			return false;
		return true;
	}
	
	
}
