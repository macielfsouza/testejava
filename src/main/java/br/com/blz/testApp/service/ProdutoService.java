package br.com.blz.testApp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.blz.testApp.model.Produto;
import br.com.blz.testApp.model.Warehouse;
import br.com.blz.testApp.repository.ProdutoRepository;

@Service
public class ProdutoService {
	
	ProdutoRepository produtoRepository;
	
	@Autowired
	public ProdutoService(ProdutoRepository produtoRepository){
		
		this.produtoRepository = produtoRepository;
	}
	
	public boolean add(Produto produto){
		
		boolean boResult = false;
		
		if(produto!=null){
			
			produto.setMarketable(false);
			produto.setInventory(null);
			
			produtoRepository.save(produto);
			boResult = true;
		}
		
		return boResult;
	}

	public Produto findOneBySku(Long sku) {
		
		Produto objRetorno = produtoRepository.findOneBySku(sku);
			
		long somaWarehouse = 0;
		
		for(Warehouse ware : objRetorno.getInventory().getWarehouse())
		{
			somaWarehouse += ware.getQuantity();
		}
		
		objRetorno.getInventory().setQuantity(somaWarehouse);
		
		if(objRetorno.getInventory().getQuantity() > 0)
		{
			objRetorno.setMarketable(true);
		}
		
		
		return objRetorno;
	}
	
	public boolean deleteBySku(Long sku){
		
		boolean boResult = false;
		
		if(sku!=null){
			
			produtoRepository.deleteBySku(sku);
			boResult = true;
		}
		
		return boResult;
		
	}
	

}
