package br.com.blz.testApp.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.blz.testApp.model.Produto;
import br.com.blz.testApp.service.ProdutoService;

@Controller
@RequestMapping("/produtos")
public class ProdutoController {
	
	@Autowired
	private ProdutoService produtoService;
	
	
	@RequestMapping(method = RequestMethod.POST)
	public boolean add(@RequestBody Produto produto)
	{
		boolean boResult = produtoService.add(produto);
		
		return boResult;
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Produto findOneBySku(Long sku){
		
		Produto objRetorno = produtoService.findOneBySku(sku);
		
		return objRetorno;
	}
	
	
	
	@RequestMapping(value = "produtos/{id}", method = RequestMethod.DELETE)
	public boolean deleteBySku(Long sku)
	{
		boolean boResult = produtoService.deleteBySku(sku);
		
		return boResult;
	}

}
