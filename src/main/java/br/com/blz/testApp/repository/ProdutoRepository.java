package br.com.blz.testApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.blz.testApp.model.Produto;

@Transactional
public interface ProdutoRepository extends JpaRepository<Produto,Long>{
	
	public Produto findOneBySku(Long sku);
	
	public void deleteBySku(Long sku);

}
